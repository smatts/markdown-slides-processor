FROM pandoc/latex:3.1.1

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN apk add --update ghostscript
RUN apk add --update npm
RUN apk add --update git
RUN npm install npx
RUN apk add --update chromium
RUN python3 -m ensurepip
RUN pip3 install --upgrade pip
RUN apk add --update jq
COPY requirements.txt .
RUN pip3 install -r requirements.txt

ENV MD_INPUT_DIR=
WORKDIR /build

COPY process.sh .
COPY pandoc-preparation.sh .
COPY create_slides_helper.sh .
COPY pandoc_generate.sh .
COPY helper.py .
COPY create-metadata-files.py .
COPY default-pandoc.css .
COPY Manrope-Medium.ttf .
COPY default-config.yml .
COPY template.html .
COPY template-de.yml .
COPY template-en.yml .
COPY labels labels

ENTRYPOINT ["/build/process.sh"]
