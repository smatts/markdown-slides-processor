#!/bin/sh

WORKDIR=/build

pandoc title.txt index.md -o index.html --embed-resources --standalone --template $WORKDIR/template.html --css pandoc.css
cp index.html public/index.html
ls -al
ls -al public/