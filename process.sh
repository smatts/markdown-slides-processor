#!/bin/sh

WORKDIR=/build

$WORKDIR/pandoc-preparation.sh
$WORKDIR/create_slides_helper.sh
if [ -n "$MD_INPUT_DIR" ]; then
  cd "$MD_INPUT_DIR" || exit 1
fi
$WORKDIR/pandoc_generate.sh
